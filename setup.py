from setuptools import setup, find_packages

from fox_utils import __version__

setup(
    name='FoxUtils',
    version=__version__,
    packages=find_packages(),

    # metadata to display on PyPI
    author='Florian Schweikert',
    author_email='florian@fox.co.at',
    description='Django utils',
    license='MIT',
)
