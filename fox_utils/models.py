from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


class TimeStampedModel(models.Model):
    """ TimeStampedModel
    An abstract base class model that provides self-managed "created" and
    "modified" fields.
    """
    created = models.DateTimeField(_('created'), auto_now_add=True)
    modified = models.DateTimeField(_('modified'), auto_now=True)

    class Meta:
        get_latest_by = 'modified'
        ordering = ('-modified', '-created',)
        abstract = True


class AdminUrlMixin:
    """
    Add get_admin_change_url method
    """

    def get_admin_change_url(self):
        return reverse('admin:{0.app_label}_{0.model_name}_change'.format(self._meta),
                       args=[self.pk])
