import sys
from pathlib import Path


def get_logging_config(logpath, appname, debug=False, use_sentry=False):
    logfile_path = Path(logpath)
    logfile = str(logfile_path/'django_{}.log'.format(appname))
    debuglogfile = str(logfile_path/'django_{}_debug.log'.format(appname))

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': True,
        'root': {
            'level': 'DEBUG',
            'handlers': ['file', 'debugfile'],
        },
        'formatters': {
            'verbose': {
                'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
                'datefmt': "%d/%b/%Y %H:%M:%S"
            },
        },
        'handlers': {
            'file': {
                'level': 'INFO',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': logfile,
                'formatter': 'verbose',
                'encoding': 'utf-8'
            },
            'stdout': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'stream': sys.stdout
            },
            'debugfile': {
                'level': 'DEBUG',
                'class': 'logging.handlers.WatchedFileHandler',
                'filename': debuglogfile,
                'formatter': 'verbose',
                'encoding': 'utf-8'
            },
        },
        'loggers': {
            '': {
                'handlers': ['file', 'debugfile'],
                'level': 'DEBUG',
                'propagate': True,
            },
            'django.request': {
                'handlers': ['file'],
                'level': 'ERROR',
                'propagate': True,
            },
        }
    }

    if debug:
        LOGGING['disable_existing_loggers'] = False

    if use_sentry:
        LOGGING['handlers']['sentry'] = {
            'level': 'WARNING',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
        }
        LOGGING['root']['handlers'].append('sentry')

    return LOGGING
