# adapted from django/contrib/admin/templatetags/admin_list.py

from django.template import Library

register = Library()


@register.simple_tag()
def page_range(paginator, page_num):
    """
    Generate the series of links to the pages in a paginated list.
    """
    ON_EACH_SIDE = 3
    ON_ENDS = 3

    # If there are 10 or fewer pages, display links to every page.
    # Otherwise, do some fancy
    if paginator.num_pages <= 15:
        page_range = range(1, paginator.num_pages+1)
    else:
        # Insert "smart" pagination links, so that there are always ON_ENDS
        # links at either end of the list of pages, and there are always
        # ON_EACH_SIDE links at either end of the "current page" link.
        page_range = []
        if page_num > (ON_EACH_SIDE + ON_ENDS):
            page_range.extend(range(1, ON_ENDS+1))
            page_range.append(None)
            page_range.extend(range(page_num - ON_EACH_SIDE, page_num + 1))
        else:
            page_range.extend(range(1, page_num + 1))
        if page_num < (paginator.num_pages - ON_EACH_SIDE - ON_ENDS - 1):
            page_range.extend(range(page_num + 1, page_num + ON_EACH_SIDE + 1))
            page_range.append(None)
            page_range.extend(range(paginator.num_pages - ON_ENDS, paginator.num_pages+1))
        else:
            page_range.extend(range(page_num + 1, paginator.num_pages))

    return page_range
